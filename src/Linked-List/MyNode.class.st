"
A LinkedList node.
Each list has a value and is pointed to another one.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	next:		<Object>
	value:		<Object>


    Implementation Points
"
Class {
	#name : #MyNode,
	#superclass : #Object,
	#instVars : [
		'value',
		'next'
	],
	#category : #'Linked-List'
}

{ #category : #'instance creation' }
MyNode class >> with: aValue [ 
	|node|
	node := MyNode new value: aValue.
	^ node.
]

{ #category : #testing }
MyNode >> hasNext [
	^ next notNil 
]

{ #category : #accessing }
MyNode >> next [
	^ next.
]

{ #category : #accessing }
MyNode >> next: aMyNode [ 
	next := aMyNode
]

{ #category : #accessing }
MyNode >> value [ 
	^ value.
]

{ #category : #accessing }
MyNode >> value: anInteger [ 
	value := anInteger
]

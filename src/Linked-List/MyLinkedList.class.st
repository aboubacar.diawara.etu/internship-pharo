"
I represente a simple linked list. 

A linear collection of data elements.
Each data points to the next.

Internal Representation and Key Implementation Points.

    Instance Variables
	head:		<Object>
	tail:		<Object>


    Implementation Points
"
Class {
	#name : #MyLinkedList,
	#superclass : #Object,
	#instVars : [
		'head',
		'tail'
	],
	#category : #'Linked-List'
}

{ #category : #'instance creation' }
MyLinkedList class >> with: aCollection [
	|newLinkedList|
	newLinkedList := self new.
	aCollection do: [ :each | newLinkedList add: (MyNode with: each) ].
	
	^ newLinkedList .
]

{ #category : #adding }
MyLinkedList >> add: aMyNode [ 
	self isEmpty 
		ifTrue: [ head := aMyNode . tail := aMyNode ] 
		ifFalse: [ tail next: aMyNode . tail := aMyNode ]
]

{ #category : #comparing }
MyLinkedList >> contains: aMyNode [

	|node|
	
	self isEmpty ifTrue: [ ^ false ].
	
	node := self head.
	[ node = aMyNode ] whileFalse: [ node := node next].
	
	^ node isNotNil.
]

{ #category : #accessing }
MyLinkedList >> head [
	^ head
]

{ #category : #testing }
MyLinkedList >> isEmpty [
	^ head isNil and: tail isNil
]

{ #category : #accessing }
MyLinkedList >> length [
	|size node|
	size := 0.
	node := self head.
	[ node isNotNil ] whileTrue: [ size := size + 1 . node := node next ].

	^ size.
]

{ #category : #printing }
MyLinkedList >> printOn: aStream [
	
	|node|
	self isEmpty
		ifTrue: [ 
				aStream nextPutAll: 'Empty'
			] 
		ifFalse: [  
				node := self head.
				[ node hasNext ] whileTrue: [ 
							aStream nextPutAll: node value printString.
							aStream nextPutAll: ' -> ' .
							node := node next
						].
				aStream nextPutAll: (node value) printString. 
			]
]

{ #category : #accessing }
MyLinkedList >> removeAt: aPosition [ 
	|node temp|
	
	1 to: (aPosition-1) do: [ :_ | 
			node := self head.
		].
	
	temp := node next.
	node next: (temp next).
	
]

{ #category : #accessing }
MyLinkedList >> tail [
	^ tail
]

Class {
	#name : #MyLinkedListTest,
	#superclass : #TestCase,
	#category : #'Linked-List'
}

{ #category : #tests }
MyLinkedListTest >> testAddingANodeToLinkedList [

	| linkedList node head tail |
	linkedList := MyLinkedList new.
	node := MyNode new value: 7.

	"precondition: the list is empty"
	self assert: linkedList isEmpty.

	"adding new node"
	linkedList add: node.

	"the list is not empty at this point"
	self deny: linkedList isEmpty.

	"because the list contains only 1 element, the head and the tail are identic"
	head := linkedList head.
	tail := linkedList tail.
	self assert: head identicalTo: tail
]

{ #category : #tests }
MyLinkedListTest >> testAddingSeveralNodesToLinkedList [

	| linkedList lastAddedNode|
	linkedList := MyLinkedList new.
	MyNode new value: 7.

	"precondition: the list is empty"
	self assert: linkedList isEmpty.

	"adding some nodes"
	1 to: 9 do: [ :value | linkedList add: (MyNode new value: value) ].
	lastAddedNode := MyNode new value: 10.
	linkedList add: lastAddedNode.

	"the list is not empty at this point"
	self deny: linkedList isEmpty.

	"the tail is the last added node"
	self assert: lastAddedNode identicalTo: linkedList tail.
]

{ #category : #'as yet unclassified' }
MyLinkedListTest >> testCreationListFromCollection [
	|list|
	list := MyLinkedList with: #(1 3 5 9 0 1).
	self deny: list isEmpty .
]

{ #category : #tests }
MyLinkedListTest >> testCreationOfEmptyList [
	|linkedList llHead llTail|
	linkedList := MyLinkedList new.
	llHead := linkedList head.
	llTail := linkedList tail.
	self assert: (llHead isNil and: llTail isNil).
]

{ #category : #'as yet unclassified' }
MyLinkedListTest >> testListSize [
	|list|
	list := MyLinkedList new.
	"a non initilized list size is 0"
	self assert: list length equals: 0.
	
	list := MyLinkedList with: #(1 2 3 4 5 6 7).
	self assert: list length equals: 7.
]

{ #category : #'as yet unclassified' }
MyLinkedListTest >> testRemovingANodeFromList [
	|list|
	
	list := MyLinkedList with: #(1 2 3 4 5 6 7).
	self assert: list length equals: 7.
	
	list removeAt: 2.
	self assert: list length equals: 6.
	
]

{ #category : #'as yet unclassified' }
MyLinkedListTest >> testSearchingInList [
	|list node|	
	list := MyLinkedList new. 
	node := MyNode with: 7.
	self deny: (list contains: node).
	
	list add: node.
	self assert: (list contains: node). 
]

{ #category : #tests }
MyLinkedListTest >> testtPrinting [
	self assert: (MyLinkedList with: #(1 2 3 4) ) printString equals: '1 -> 2 -> 3 -> 4'.
	self assert: (MyLinkedList with: #(1)) printString equals: '1'.
	self assert: (MyLinkedList new) printString equals: 'Empty'
]

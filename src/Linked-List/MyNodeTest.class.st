Class {
	#name : #MyNodeTest,
	#superclass : #TestCase,
	#category : #'Linked-List'
}

{ #category : #tests }
MyNodeTest >> testAddingNextNode [
	|node nextNode|
	node := MyNode new value:1.
	nextNode := MyNode new value:2.
	
	"precondition, the node doesn't point to any node"
	self deny: node hasNext.
	
	"here we point it to a next node"
	node next: nextNode.
	
	"now the node has a next node"
	self assert: node hasNext.
]

{ #category : #tests }
MyNodeTest >> testCreationNode [
	1 to: 10 do: [ :_ | 
		|n| 
		n := 10 atRandom.
		self assert: (MyNode with:n) value equals: n.
		]
]

{ #category : #tests }
MyNodeTest >> testInitialisationOfNode [

	|node|
	node := MyNode new.
	node value: 9.
	self assert: node value equals: 9.
]

# Internship pharo

## Linked List
Implementation of a linked list with TDD.

![](media/tdd.gif)
### Representation
- MyLinkedList
    - Properties
        - head
        - tail (adding this one reduces the runtime of adding an element at the end)
	- Operation
		- add: adding an element at the end of the list.
		- contains: search operation of an element in the list.
		- length: compute the size of the list.
		- isEmpty: test if the list is is empty or not.
		- pretty display (n1 -> n2 -> n3)
- MyNode
	- Properties
		- value: the value of the node.
		- next: the node to wich the current node points.
		- 
![](./media/list.gif)
## Small javadoc
Display details about a class in html format.
The app has been build with the web framework `Seaside`.

![](media/javadoc.gif)

### Improvement idea.
- Displaying methods and class field details.
- Adding some style.